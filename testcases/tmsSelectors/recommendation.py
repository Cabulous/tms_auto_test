from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

from testcases.tmsSelectors.baseSelector import BaseSelector

from util import util

logger = util.get_logger()


class Recommendation(BaseSelector):
    main = (By.CLASS_NAME, 'ht-tms__recommendation-popup')
    options = (By.CLASS_NAME, 'ht-tms__recommendation-popup__button--change-view')
    dropdown = (By.CSS_SELECTOR, '.ht-tms__recommendation-popup__view-wrapper .ht-tms__custom-dropdown__item--active')
    submit_recommendation = (
        By.CSS_SELECTOR,
        '.ht-tms__recommendation-popup__view--recommendation .ht-tms__recommendation-popup__button--submit'
    )
    submit_options = (
        By.CSS_SELECTOR,
        '.ht-tms__recommendation-popup__view--full-list .ht-tms__recommendation-popup__button--submit'
    )

    cookie_close = 'tms_close_recommendation'

    def __init__(self, driver: WebDriver):
        BaseSelector.__init__(self, driver)
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 3)
        self.action = ActionChains(self.driver)

    def wait_until_presents(self):
        self.wait.until(ec.presence_of_element_located(self.main))

    def go_recommendation(self):
        self.click(self.submit_recommendation)

    def select_region(self, region_trigger: WebElement, region: str):
        option = f'.ht-tms__recommendation-popup .ht-tms__custom-dropdown__item[data-value="{region}"]'
        region_trigger.click()
        self.click((By.CSS_SELECTOR, option))

    def select_locale(self, locale_trigger: WebElement, locale: str):
        option = f'.ht-tms__recommendation-popup .ht-tms__custom-dropdown__item[data-value="{locale}"]'
        locale_trigger.click()
        self.click((By.CSS_SELECTOR, option))

    def get_triggers(self):
        dropdowns = self.find_elements(self.dropdown)

        if len(dropdowns) != 2:
            logger.error(f'Expect 2 dropdowns but got {len(dropdowns)}')
            assert False

        return dropdowns

    def go_more_options(self, locale='', region=''):
        self.click(self.options)

        region_trigger, locale_trigger = self.get_triggers()
        if region:
            self.select_region(region_trigger, region)
        if locale:
            self.select_locale(locale_trigger, locale)

        self.click(self.submit_options)

    def delete_cookie(self):
        cookie = self.driver.get_cookie(self.cookie_close)
        self.driver.delete_cookie(self.cookie_close)
        logger.debug(f'Remove recommendation cookie: {cookie}')
