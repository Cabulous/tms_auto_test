from selenium.webdriver.common.by import By

from testcases.tmsSelectors.baseSelector import BaseSelector

from time import sleep

from util import util

logger = util.get_logger()


def wait_for_render():
    sleep(1)


class LocaleAndRegionSelector(BaseSelector):
    region_trigger = (By.CSS_SELECTOR, '.ht-tms__form__dropdown-wrapper--region .ht-tms__custom-dropdown__item--active')
    locale_trigger = (By.CSS_SELECTOR, '.ht-tms__form__dropdown-wrapper--locale .ht-tms__custom-dropdown__item--active')
    submit_btn = (By.CLASS_NAME, 'ht-tms__form__button')

    def __init__(self, driver):
        BaseSelector.__init__(self, driver)

    def open_dropdown_selector(self):
        self.hover_selector()
        logger.debug('Open locale-region dropdown selector')

    def select_region(self, region: str):
        option = f'.ht-tms__custom-dropdown__item[data-value="{region.upper()}"]'

        self.click(self.region_trigger)
        wait_for_render()

        self.click((By.CSS_SELECTOR, option))
        logger.debug(f'Click region {region}')

    def select_locale(self, locale: str):
        option = f'.ht-tms__custom-dropdown__item[data-value="{locale}"]'

        self.click(self.locale_trigger)
        wait_for_render()

        self.click((By.CSS_SELECTOR, option))
        logger.debug(f'Click locale {locale}, selector {option}')

    def goto(self, locale: str, region: str):
        self.hover_selector()
        self.select_locale(locale)
        self.select_region(region)
        self.click(self.submit_btn)

    def delete_cookie(self):
        self.delete_region_cookie()
        self.delete_locale_cookie()
