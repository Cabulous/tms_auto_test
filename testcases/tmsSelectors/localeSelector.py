from selenium.webdriver.common.by import By

from testcases.tmsSelectors.baseSelector import BaseSelector

from util import util

logger = util.get_logger()


class LocaleSelector(BaseSelector):

    def __init__(self, driver):
        BaseSelector.__init__(self, driver)

    def goto(self, locale: str):
        self.hover_selector()
        self.click((By.ID, locale))
        logger.debug(f'Click locale {locale}')

    def delete_cookie(self):
        self.delete_locale_cookie()
