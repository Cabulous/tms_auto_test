from typing import List, Tuple

from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from util import util

logger = util.get_logger()


class BaseSelector(object):
    selector_main = (By.CLASS_NAME, 'ht-tms-dropdown')

    region_cookie = 'tms_default_country_code'
    locale_cookie = 'tms_default_locale'

    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 3)
        self.action = ActionChains(self.driver)

    def find_element(self, loc: Tuple[str, str]) -> WebElement:
        return self.driver.find_element(*loc)

    def find_elements(self, loc: Tuple[str, str]) -> List[WebElement]:
        return self.driver.find_elements(*loc)

    def click(self, loc: Tuple[str, str]) -> None:
        self.driver.find_element(*loc).click()

    def move_to(self, loc: Tuple[str, str]) -> None:
        element = self.find_element(loc)
        self.action.move_to_element(element).perform()

    def get_selector(self) -> WebElement:
        return self.find_element(self.selector_main)

    def hover_selector(self) -> None:
        self.move_to(self.selector_main)

    def click_selector(self) -> None:
        self.click(self.selector_main)

    def delete_selector_cookie(self, cookie_name: str):
        cookie = self.driver.get_cookie(cookie_name)
        self.driver.delete_cookie(cookie_name)
        logger.debug(f'Remove cookie: {cookie}')

    def delete_region_cookie(self):
        self.delete_selector_cookie(self.region_cookie)

    def delete_locale_cookie(self):
        self.delete_selector_cookie(self.locale_cookie)

    def wait_until_selector_presents(self):
        self.wait.until(ec.presence_of_element_located(self.selector_main))
