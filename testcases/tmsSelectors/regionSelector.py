from selenium.webdriver.common.by import By

from testcases.tmsSelectors.baseSelector import BaseSelector

from util import util

logger = util.get_logger()


class RegionSelector(BaseSelector):

    def __init__(self, driver):
        BaseSelector.__init__(self, driver)

    def goto(self, region: str):
        self.hover_selector()
        self.click((By.ID, region))
        logger.debug(f'Click region {region} in the selector')

    def delete_cookie(self):
        self.delete_region_cookie()
