import pytest

from selenium import webdriver

from testcases.shopify.pageNavigator import PageNavigator
from testcases.shopify.pageTester import PageTester
from testcases.tmsSelectors.localeAndRegionSelector import LocaleAndRegionSelector
from testcases.tmsSelectors.recommendation import Recommendation

from util import util


class TestLocaleAndRegionRecommendation:

    def setup_class(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('prefs', {'intl.accept_languages': util.get_browser_locale()})
        self.driver = webdriver.Chrome(options=options)
        self.selector = LocaleAndRegionSelector(self.driver)
        self.recommendation = Recommendation(self.driver)
        self.page_tester = PageTester(self.driver)
        self.page = PageNavigator(self.driver)
        self.page.goto_home_with_url()

    def setup(self):
        self.selector.delete_cookie()
        self.recommendation.delete_cookie()

    @pytest.mark.parametrize('browser_locale, geo_region', [(util.get_browser_locale(), util.get_geo_region())])
    def test_switch_locale_and_region_with_recommendation(self, browser_locale, geo_region):
        self.page.goto_home_with_url()
        self.recommendation.wait_until_presents()
        self.recommendation.go_recommendation()
        self.page_tester.run(browser_locale, geo_region)

    @pytest.mark.parametrize('locale, region', util.get_combinations_test_data())
    def test_switch_locale_and_region_with_options(self, locale, region):
        self.page.goto_home_with_url()
        self.recommendation.wait_until_presents()
        self.recommendation.go_more_options(locale, region)
        self.page_tester.run(locale, region)

    def teardown_class(self):
        self.driver.quit()
