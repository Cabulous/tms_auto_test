import pytest

from selenium import webdriver

from testcases.shopify.pageNavigator import PageNavigator
from testcases.shopify.pageTester import PageTester
from testcases.tmsSelectors.localeSelector import LocaleSelector
from testcases.tmsSelectors.recommendation import Recommendation

from util import util


class TestLocaleSelectorRecommendation:

    def setup_class(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('prefs', {'intl.accept_languages': util.get_browser_locale()})
        self.driver = webdriver.Chrome(options=options)
        self.selector = LocaleSelector(self.driver)
        self.recommendation = Recommendation(self.driver)
        self.page_tester = PageTester(self.driver)
        self.page = PageNavigator(self.driver)
        self.page.goto_home_with_url()

    def setup(self):
        self.selector.delete_cookie()
        self.recommendation.delete_cookie()

    @pytest.mark.parametrize('browser_locale, base_region', [(util.get_browser_locale(), util.get_base_region())])
    def test_switch_locale_with_recommendation(self, browser_locale, base_region):
        self.page.goto_home_with_url()
        self.recommendation.wait_until_presents()
        self.recommendation.go_recommendation()
        self.page_tester.run(browser_locale, base_region)

    # TODO To write this test, add a CSS class to separate region dropdown and locale dropdown
    # @pytest.mark.parametrize('locale, base_region', util.get_locale_test_data())
    # def test_switch_locale_with_options(self, locale, base_region):
    #     self.page.goto_home_with_url()
    #     self.recommendation.wait_until_presents()
    #     self.recommendation.go_more_options(locale)
    #     self.page_tester.run(locale, base_region)

    def teardown_class(self):
        self.driver.quit()
