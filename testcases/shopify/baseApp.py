from typing import Tuple

from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement


class BaseApp(object):

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def find_element(self, loc: Tuple[str, str]) -> WebElement:
        return self.driver.find_element(*loc)

    def click(self, loc: Tuple[str, str]) -> None:
        self.driver.find_element(*loc).click()

    def get_value(self, loc: Tuple[str, str]) -> str:
        return self.driver.find_element(*loc).get_attribute('value')
