import json

from selenium import webdriver

from settings import CONFIG_FILE

from testcases.shopify.minimalTheme import MinimalTheme

from util import util

logger = util.get_logger()


class PageNavigator:

    def __init__(self, driver: webdriver):
        self.driver = driver
        self.theme = MinimalTheme(self.driver)

        with open(CONFIG_FILE) as f:
            data = json.load(f)
            store = data['store']
            self.root_url = store['root']
            self.collection_handle = store['collection_handle']
            self.product_handle = store['product_handle']

    def goto_home_with_nav(self):
        logger.debug('Go to home page with navigation')
        self.theme.goto_home_with_nav()

    def goto_collection_with_nav(self):
        logger.debug('Go to collection page with navigation')
        self.theme.goto_collection_with_nav()

    def goto_product_with_nav(self):
        logger.debug('Go to product page with navigation')
        self.theme.goto_product_with_nav()

    def goto_home_with_url(self, locale=''):
        logger.debug(f'Go to home page with url, locale: {locale if locale else None}')
        home_page = self.root_url
        if len(locale) > 0:
            home_page = f'{self.root_url}/{locale.lower()}'
        self.driver.get(home_page)

    def goto_example_collection_with_url(self, locale=''):
        logger.debug(f'Go to collection page with url, locale: {locale if locale else None}')
        collection_page = f'{self.root_url}/collections/{self.collection_handle}'
        if len(locale) > 0:
            collection_page = f'{self.root_url}/{locale}/collections/{self.collection_handle}'
        self.driver.get(collection_page)

    def goto_example_product_with_url(self, locale=''):
        logger.debug(f'Go to product page with url, locale: {locale if locale else None}')
        product_page = f'{self.root_url}/products/{self.product_handle}'
        if len(locale) > 0:
            product_page = f'{self.root_url}/{locale}/products/{self.product_handle}'
        self.driver.get(product_page)

    def goto_locale_with_url(self, locale: str):
        logger.debug(f'Go to current page with url, locale: {locale if locale else None}')
        handle = self.get_shopify_page_handle()
        target_url = f'{self.root_url}/{locale.lower()}/{handle}'
        self.driver.get(target_url)

    def get_shopify_root(self) -> str:
        return self.driver.execute_script("return window.Shopify.routes.root")

    def get_shopify_page_handle(self) -> str:
        shopify_root = self.get_shopify_root()
        parts = self.driver.current_url.split(shopify_root)
        return '/'.join(parts[3:])
