from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

from util import util
from testcases.shopify.pageNavigator import PageNavigator

logger = util.get_logger()


class PageTester:
    def __init__(self, driver: webdriver):
        self.driver = driver
        self.action = ActionChains(self.driver)
        self.page = PageNavigator(self.driver)

        self.expect_locale = None
        self.expect_region = None
        self.curr_locale = None
        self.curr_region = None

    def run(self, expect_locale: str, expect_region: str):
        self.expect_locale, self.expect_region = expect_locale, expect_region

        util.wait_for_redirection_done()

        self.check()

        self.page.goto_home_with_nav()
        self.check()

        self.page.goto_collection_with_nav()
        self.check()

        self.page.goto_product_with_nav()
        self.check()

    def check(self):
        try:
            self.curr_locale = self.driver.execute_script("return window.Shopify.locale")
            logger.debug('Verify locale')
            assert self.expect_locale == self.curr_locale
        except AssertionError:
            logger.error(f'Expect locale {self.expect_locale} but got current locale {self.curr_locale}')
            assert False

        try:
            self.curr_region = self.driver.execute_script("return window.Shopify.country")
            logger.debug('Verify region')
            assert self.expect_region == self.curr_region
        except AssertionError:
            logger.error(f'Expect locale {self.expect_region} but got current region {self.curr_region}')
            assert False
