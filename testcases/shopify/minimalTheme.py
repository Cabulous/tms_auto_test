from selenium import webdriver
from selenium.webdriver.common.by import By

from testcases.shopify.baseTheme import BaseTheme

from util import util

logger = util.get_logger()


class MinimalTheme(BaseTheme):
    home_link = (By.CSS_SELECTOR, '.site-header__logo a[href*="/"]')
    collection_link = (By.CSS_SELECTOR, '#AccessibleNav a[href*="/collections/all"]')
    product_link = (By.CSS_SELECTOR, 'a.grid-link[href*="/products"]')

    def __init__(self, driver: webdriver):
        BaseTheme.__init__(self, driver)
        self.driver = driver

    def goto_home_with_nav(self):
        logger.debug('Go to home page with navigation')
        self.click(self.home_link)

    def goto_collection_with_nav(self):
        logger.debug('Go to collection page with navigation')
        self.click(self.collection_link)

    def goto_product_with_nav(self):
        logger.debug('Go to product page with navigation')
        self.click(self.product_link)
