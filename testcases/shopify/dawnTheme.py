from selenium import webdriver
from selenium.webdriver.common.by import By

from testcases.shopify.baseTheme import BaseTheme

from util import util

logger = util.get_logger()


class DawnTheme(BaseTheme):
    home_link = (By.CSS_SELECTOR, 'a.header__heading-link')
    collection_dropdown = (By.CSS_SELECTOR, 'summary.header__menu-item')
    collection_link = (By.CSS_SELECTOR, '.header__submenu a[href*="/collections"]')
    product_link = (By.CSS_SELECTOR, '.grid__item a[href*="/products"]')

    locale_selector = (
        By.CSS_SELECTOR,
        '.localization-selector[aria-controls="FooterLanguageList"]',
    )
    region_selector = (
        By.CSS_SELECTOR,
        '.localization-selector[aria-controls="FooterCountryList"]',
    )

    def __init__(self, driver: webdriver):
        BaseTheme.__init__(self, driver)
        self.driver = driver

    def goto_home_with_nav(self):
        logger.debug('Go to home page with navigation')
        self.click(self.home_link)

    def goto_collection_with_nav(self):
        logger.debug('Go to collection page with navigation')
        self.click(self.collection_dropdown)
        self.click(self.collection_link)

    def goto_product_with_nav(self):
        logger.debug('Go to product page with navigation')
        self.click(self.product_link)

    def select_locale(self, locale: str):
        option = f'.disclosure__link[lang="{locale}"]'

        self.click(self.locale_selector)
        util.wait_for_redirection_done()

        self.click((By.CSS_SELECTOR, option))
        logger.debug(f'Select locale {locale}')

    def select_region(self, region: str):
        option = f'.disclosure__link[data-value="{region.upper()}"]'

        self.click(self.region_selector)
        util.wait_for_redirection_done()

        self.click((By.CSS_SELECTOR, option))
        logger.debug(f'Select region {region}')
