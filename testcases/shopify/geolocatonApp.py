from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait

from testcases.shopify.baseApp import BaseApp

from util import util

logger = util.get_logger()


class GeolocationApp(BaseApp):
    popup = (By.CSS_SELECTOR, '.recommendation-modal__container')
    locale_code_input = (By.CSS_SELECTOR, 'input[name="locale_code"]')
    region_code_input = (By.CSS_SELECTOR, 'input[name="country_code"]')
    button = (By.CSS_SELECTOR, '.recommendation-modal__button')

    more_button = (By.CSS_SELECTOR, '.recommendation-modal__button--minimal')
    locale_selector = (
        By.CSS_SELECTOR,
        '.recommendation-modal__selector[name="locale_code"]',
    )
    region_selector = (
        By.CSS_SELECTOR,
        '.recommendation-modal__selector--flag[name="country_code"]',
    )

    def __init__(self, driver: webdriver):
        BaseApp.__init__(self, driver)
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 3)

    def wait_until_presents(self) -> bool:
        logger.debug('Waiting until Geolocation app presents')
        try:
            self.wait.until(ec.visibility_of_element_located(self.popup))
            logger.debug('Geolocation app presents')
            return True
        except TimeoutException:
            logger.error('Cannot find Geolocation App popup')
            return False

    def submit_change(self):
        logger.debug('Click submit button')
        self.click(self.button)

    def goto_more_options(self):
        logger.debug('Click more-options button')
        self.click(self.more_button)

    def select_locale(self, locale: str):
        logger.debug('Open locale selector')
        select = Select(self.find_element(self.locale_selector))
        logger.debug(f'Select locale {locale}')
        select.select_by_value(locale)

    def select_region(self, region: str):
        logger.debug('Open region selector')
        select = Select(self.find_element(self.region_selector))
        logger.debug(f'Select region {region}')
        select.select_by_value(region)

    def get_locale(self):
        return self.get_value(self.locale_code_input)

    def get_region(self):
        return self.get_value(self.region_code_input)
