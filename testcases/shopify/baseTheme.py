from typing import Tuple

from selenium.webdriver.chrome.webdriver import WebDriver


class BaseTheme(object):

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def click(self, loc: Tuple[str, str]) -> None:
        self.driver.find_element(*loc).click()
