import pytest

from selenium import webdriver

from testcases.shopify.pageNavigator import PageNavigator
from testcases.shopify.pageTester import PageTester
from testcases.tmsSelectors.regionSelector import RegionSelector

from util import util


class TestRegionSelectorShopBase:

    def setup_class(self):
        self.driver = webdriver.Chrome()
        self.page_tester = PageTester(self.driver)
        self.selector = RegionSelector(self.driver)
        self.page = PageNavigator(self.driver)
        self.page.goto_home_with_url()

    def setup(self):
        self.selector.delete_cookie()

    @pytest.mark.parametrize('base_locale, base_region', [(util.get_base_locale(), util.get_base_region())])
    def test_base_region_on_home_page(self, base_locale, base_region):
        self.page.goto_home_with_url()
        self.page_tester.run(base_locale, base_region)

    @pytest.mark.parametrize('base_locale, base_region', [(util.get_base_locale(), util.get_base_region())])
    def test_base_region_on_collection_page(self, base_locale, base_region):
        self.page.goto_example_collection_with_url()
        self.page_tester.run(base_locale, base_region)

    @pytest.mark.parametrize('base_locale, region', util.get_region_test_data())
    def test_switch_region_with_selector(self, base_locale, region):
        self.page.goto_home_with_url()
        self.selector.goto(region)
        self.page_tester.run(base_locale, region)

    def teardown_class(self):
        self.driver.quit()
