import pytest

from selenium import webdriver

from testcases.shopify.pageNavigator import PageNavigator
from testcases.shopify.pageTester import PageTester
from testcases.tmsSelectors.localeSelector import LocaleSelector
from testcases.tmsSelectors.recommendation import Recommendation

from util import util


class TestRegionSelectorRecommendation:

    def setup_class(self):
        self.driver = webdriver.Chrome()
        self.selector = LocaleSelector(self.driver)
        self.recommendation = Recommendation(self.driver)
        self.page_tester = PageTester(self.driver)
        self.page = PageNavigator(self.driver)
        self.page.goto_home_with_url()

    def setup(self):
        self.selector.delete_cookie()
        self.recommendation.delete_cookie()

    @pytest.mark.parametrize('base_locale, geo_region', [(util.get_base_locale(), util.get_geo_region())])
    def test_switch_locale_with_recommendation(self, base_locale, geo_region):
        self.page.goto_home_with_url()
        self.recommendation.wait_until_presents()
        self.recommendation.go_recommendation()
        self.page_tester.run(base_locale, geo_region)

    # TODO To write this test, add a CSS class to separate region dropdown and locale dropdown
    # @pytest.mark.parametrize('base_locale, region', util.get_region_test_data())
    # def test_switch_region_with_options(self, base_locale, region):
    #     self.page.goto_home_with_url()
    #     self.recommendation.wait_until_presents()
    #     self.recommendation.go_more_options(region=region)
    #     self.page_tester.run(base_locale, region)

    def teardown_class(self):
        self.driver.quit()
