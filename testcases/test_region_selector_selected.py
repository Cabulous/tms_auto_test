import pytest

from selenium import webdriver

from testcases.shopify.pageNavigator import PageNavigator
from testcases.shopify.pageTester import PageTester
from testcases.tmsSelectors.regionSelector import RegionSelector

from util import util


class TestRegionSelectorSelected:

    def setup_class(self):
        self.driver = webdriver.Chrome()
        self.selector = RegionSelector(self.driver)
        self.page_tester = PageTester(self.driver)
        self.page = PageNavigator(self.driver)
        self.page.goto_home_with_url()

    def setup(self):
        self.selector.delete_cookie()

    @pytest.mark.parametrize('base_locale, selected_region', [(util.get_base_locale(), util.get_selected_region())])
    def test_selected_region_on_home_page(self, base_locale, selected_region):
        self.start_with_home_page()
        self.page_tester.run(base_locale, selected_region)

    @pytest.mark.parametrize('base_locale, selected_region', [(util.get_base_locale(), util.get_selected_region())])
    def test_selected_region_on_collection_page(self, base_locale, selected_region):
        self.start_with_example_collection_page()
        self.page_tester.run(base_locale, selected_region)

    @pytest.mark.parametrize('base_locale, region', util.get_region_test_data())
    def test_switch_region_with_selector(self, base_locale, region):
        self.start_with_home_page()
        self.selector.goto(region)
        self.page_tester.run(base_locale, region)

    @pytest.mark.parametrize('base_locale, region', util.get_region_test_data())
    def test_switch_region_with_selector(self, base_locale, region):
        self.start_with_example_collection_page()
        self.selector.goto(region)
        self.page_tester.run(base_locale, region)

    def start_with_home_page(self):
        self.page.goto_home_with_url()
        util.wait_for_redirection_done()

    def start_with_example_collection_page(self):
        self.page.goto_example_collection_with_url()
        util.wait_for_redirection_done()

    def teardown_class(self):
        self.driver.quit()
