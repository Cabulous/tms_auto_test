import pytest

from selenium import webdriver

from testcases.shopify.dawnTheme import DawnTheme
from testcases.shopify.pageNavigator import PageNavigator
from testcases.shopify.pageTester import PageTester
from testcases.tmsSelectors.localeAndRegionSelector import LocaleAndRegionSelector

from util import util


class TestThemeDefaultSelector:

    def setup_class(self):
        self.driver = webdriver.Chrome()
        self.selector = LocaleAndRegionSelector(self.driver)
        self.theme = DawnTheme(self.driver)
        self.page_tester = PageTester(self.driver)
        self.page = PageNavigator(self.driver)
        self.page.goto_home_with_url()

    def setup(self):
        self.selector.delete_cookie()

    @pytest.mark.parametrize('locale, base_region', util.get_locale_test_data())
    def test_locale_selector(self, locale, base_region):
        self.theme.select_locale(locale)
        self.page_tester.run(locale, base_region)

    @pytest.mark.parametrize('base_locale, region', util.get_region_test_data())
    def test_region_selector(self, base_locale, region):
        self.theme.select_region(region)
        self.page_tester.run(base_locale, region)

    def teardown_class(self):
        self.driver.quit()
