import pytest

from selenium import webdriver

from testcases.shopify.pageNavigator import PageNavigator
from testcases.tmsSelectors.localeAndRegionSelector import LocaleAndRegionSelector
from testcases.shopify.pageTester import PageTester

from util import util


class TestLocaleAndRegionSelectorBase:

    def setup_class(self):
        self.driver = webdriver.Chrome()
        self.selector = LocaleAndRegionSelector(self.driver)
        self.page_tester = PageTester(self.driver)
        self.page = PageNavigator(self.driver)
        self.page.goto_home_with_url()

    def setup(self):
        self.selector.delete_cookie()

    @pytest.mark.parametrize('base_locale, base_region', [(util.get_base_locale(), util.get_base_region())])
    def test_shop_default(self, base_locale, base_region):
        self.start_with_home_page()
        self.page_tester.run(base_locale, base_region)

    @pytest.mark.parametrize('locale, base_region', util.get_locale_test_data())
    def test_switch_locale_with_selector(self, locale, base_region):
        self.start_with_home_page()
        self.selector.goto(locale, base_region)
        self.page_tester.run(locale, base_region)

    @pytest.mark.parametrize('locale, base_region', util.get_locale_test_data())
    def test_switch_locale_with_url(self, locale, base_region):
        self.start_with_home_page()
        self.page.goto_locale_with_url(locale)
        self.page_tester.run(locale, base_region)

    @pytest.mark.parametrize('init_locale, next_locale, base_region', util.get_switch_locale_test_data())
    def test_switch_locale_with_selector_in_alt_locale(self, init_locale, next_locale, base_region):
        self.page.goto_home_with_url(init_locale)
        self.selector.goto(next_locale, base_region)
        self.page_tester.run(next_locale, base_region)

    @pytest.mark.parametrize('init_locale, next_locale, base_region', util.get_switch_locale_test_data())
    def test_switch_locale_with_url_in_alt_locale(self, init_locale, next_locale, base_region):
        self.page.goto_home_with_url(init_locale)
        self.page_tester.run(init_locale, base_region)
        self.page.goto_home_with_url(next_locale)
        self.page_tester.run(next_locale, base_region)

    @pytest.mark.parametrize('locale, base_region', util.get_locale_test_data())
    def test_switch_locale_with_selector_on_collection_page(self, locale, base_region):
        self.page.goto_example_collection_with_url()
        self.selector.goto(locale, base_region)
        self.page_tester.run(locale, base_region)

    @pytest.mark.parametrize('locale, base_region', util.get_locale_test_data())
    def test_switch_locale_with_url_on_collection_page(self, locale, base_region):
        self.page.goto_example_collection_with_url()
        self.page.goto_locale_with_url(locale)
        self.page_tester.run(locale, base_region)

    @pytest.mark.parametrize('init_locale, next_locale, base_region', util.get_switch_locale_test_data())
    def test_switch_locale_with_selector_on_collection_page_in_alt_locale(self, init_locale, next_locale, base_region):
        self.page.goto_example_collection_with_url(init_locale)
        self.page_tester.run(init_locale, base_region)
        self.selector.goto(next_locale, base_region)
        self.page_tester.run(next_locale, base_region)

    @pytest.mark.parametrize('init_locale, next_locale, base_region', util.get_switch_locale_test_data())
    def test_switch_locale_with_url_on_collection_page_in_alt_locale(self, init_locale, next_locale, base_region):
        self.page.goto_example_collection_with_url(init_locale)
        self.page_tester.run(init_locale, base_region)
        self.page.goto_locale_with_url(next_locale)
        self.page_tester.run(next_locale, base_region)

    @pytest.mark.parametrize('base_locale, region', util.get_region_test_data())
    def test_switch_region_with_selector(self, base_locale, region):
        self.start_with_home_page()
        self.selector.goto(base_locale, region)
        self.page_tester.run(base_locale, region)

    def start_with_home_page(self):
        self.page.goto_home_with_url()
        util.wait_for_redirection_done()

    def teardown_class(self):
        self.driver.quit()
