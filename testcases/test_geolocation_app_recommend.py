from selenium import webdriver

from testcases.shopify.geolocatonApp import GeolocationApp
from testcases.shopify.pageNavigator import PageNavigator
from testcases.shopify.pageTester import PageTester

from util import util


class TestGeolocationAppRecommend:

    def setup_class(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('prefs', {'intl.accept_languages': util.get_browser_locale()})
        self.driver = webdriver.Chrome(options=options)
        self.app = GeolocationApp(self.driver)
        self.page_tester = PageTester(self.driver)
        self.page = PageNavigator(self.driver)
        self.page.goto_home_with_url()

    def setup(self):
        self.page.goto_home_with_url()

    def test_popup_recommendation(self):
        self.app.wait_until_presents()
        locale_code = self.app.get_locale()
        country_code = self.app.get_region()
        self.app.submit_change()
        self.page_tester.run(locale_code, country_code)

    def teardown_class(self):
        self.driver.quit()
