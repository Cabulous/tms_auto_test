import pytest

from selenium import webdriver

from testcases.shopify.pageNavigator import PageNavigator
from testcases.tmsSelectors.localeAndRegionSelector import LocaleAndRegionSelector
from testcases.shopify.pageTester import PageTester

from util import util


class TestLocaleAndRegionSelectorBrowserGeo:

    def setup_class(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('prefs', {'intl.accept_languages': util.get_browser_locale()})
        self.driver = webdriver.Chrome(options=options)
        self.selector = LocaleAndRegionSelector(self.driver)
        self.page_tester = PageTester(self.driver)
        self.page = PageNavigator(self.driver)
        self.page.goto_home_with_url()

    def setup(self):
        self.selector.delete_cookie()

    @pytest.mark.parametrize('browser_locale, geo_region', [(util.get_browser_locale(), util.get_geo_region())])
    def test_auto_redirect_on_home_page(self, browser_locale, geo_region):
        self.start_with_home_page()
        self.page_tester.run(browser_locale, geo_region)

    @pytest.mark.parametrize('locale, geo_region', util.get_locale_and_geo_region_test_data())
    def test_switch_locale_with_url(self, locale, geo_region):
        self.start_with_home_page()
        self.page.goto_locale_with_url(locale)
        self.page_tester.run(locale, geo_region)

    @pytest.mark.parametrize('locale, region', util.get_combinations_test_data())
    def test_switch_locale_with_selector(self, locale, region):
        self.start_with_home_page()
        self.selector.goto(locale, region)
        self.page_tester.run(locale, region)

    def start_with_home_page(self):
        self.page.goto_home_with_url()
        util.wait_for_redirection_done()

    def start_with_example_collection_page(self):
        self.page.goto_example_collection_with_url()
        util.wait_for_redirection_done()

    def teardown_class(self):
        self.driver.quit()
