import itertools
import json
from time import sleep
from typing import List, Tuple

from settings import CONFIG_FILE, LOG_DIR

import logging
import logging.handlers


def get_logger():
    logger = logging.getLogger('tms_test')
    logger.setLevel(logging.DEBUG)

    if not logger.handlers:
        rf_handler = logging.handlers.TimedRotatingFileHandler(
            f'{LOG_DIR}/all.log',
            when='midnight',
            interval=1,
            backupCount=7,
        )
        rf_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))

        f_handler = logging.FileHandler(f'{LOG_DIR}/error.log')
        f_handler.setLevel(logging.ERROR)
        f_handler.setFormatter(
            logging.Formatter('%(asctime)s - %(levelname)s - %(filename)s[:%(lineno)d] - %(message)s'))

        logger.addHandler(rf_handler)
        logger.addHandler(f_handler)

    return logger


# wait fore redirection completes
# TODO Sleep is not ideal. Need to find a better wait.
def wait_for_redirection_done():
    sleep(2)


def get_base_locale() -> str:
    with open(CONFIG_FILE) as f:
        data = json.load(f)
        return data['store']['base_locale']


def get_selected_locale() -> str:
    with open(CONFIG_FILE) as f:
        data = json.load(f)
        return data['test']['manual_selected_locale']


def get_browser_locale() -> str:
    with open(CONFIG_FILE) as f:
        data = json.load(f)
        browser_locale = data['test']['browser_based_locale']
        return browser_locale


def get_locale_test_data() -> List[Tuple[str, str]]:
    with open(CONFIG_FILE) as f:
        data = json.load(f)
        locales = data['test']['locales']
        base_region = data['store']['base_region']
        lst = []
        for locale in locales:
            lst.append((locale, base_region))
        return lst


def get_switch_locale_test_data() -> List[Tuple[str, str, str]]:
    with open(CONFIG_FILE) as f:
        data = json.load(f)
        locales = data['test']['locales']
        base_region = data['store']['base_region']
        lst = []
        for i in range(1, len(locales)):
            lst.append((locales[i - 1], locales[i], base_region))
        return lst


def get_base_region() -> str:
    with open(CONFIG_FILE) as f:
        data = json.load(f)
        return data['store']['base_region']


def get_selected_region() -> str:
    with open(CONFIG_FILE) as f:
        data = json.load(f)
        return data['test']['manual_selected_region']


def get_geo_region() -> str:
    with open(CONFIG_FILE) as f:
        data = json.load(f)
        return data['test']['geo_based_region']


def get_region_test_data() -> List[Tuple[str, str]]:
    with open(CONFIG_FILE) as f:
        data = json.load(f)
        base_locale = data['store']['base_locale']
        regions = data['test']['regions']
        lst = []
        for region in regions:
            lst.append((base_locale, region))
        return lst


def get_selected_region_test_data() -> List[Tuple[str, str]]:
    with open(CONFIG_FILE) as f:
        data = json.load(f)
        base_locale = data['store']['base_locale']
        manual_selected_region = data['test']['manual_selected_region']
        return [(base_locale, manual_selected_region)]


def get_geo_region_test_data() -> List[Tuple[str, str, str]]:
    with open(CONFIG_FILE) as f:
        data = json.load(f)
        base_locale = data['store']['base_locale']
        regions = data['test']['regions']
        geo_region = data['test']['geo_based_region']
        lst = []
        for region in regions:
            lst.append((base_locale, region, geo_region))
        return lst


def get_locale_and_selected_region_test_data() -> List[Tuple[str, str]]:
    with open(CONFIG_FILE) as f:
        data = json.load(f)
        locales = data['test']['locales']
        selected_region = data['test']['manual_selected_region']
        lst = []
        for locale in locales:
            lst.append((locale, selected_region))
        return lst


def get_locale_and_geo_region_test_data() -> List[Tuple[str, str]]:
    with open(CONFIG_FILE) as f:
        data = json.load(f)
        locales = data['test']['locales']
        geo_region = data['test']['geo_based_region']
        lst = []
        for locale in locales:
            lst.append((locale, geo_region))
        return lst


def get_combinations_test_data() -> List[Tuple[str, str]]:
    with open(CONFIG_FILE) as f:
        data = json.load(f)
        locales = data['test']['locales']
        regions = data['test']['regions']
        lst = list(itertools.product(locales, regions))
        return lst
