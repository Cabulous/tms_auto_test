# Pre-Test

1. Using the test store at `hextom-preview-2019.myshopify.com`
2. Publish **Minimal** theme
3. Turn on VPN with the **US IP** address

# Locale Selector

## **Base Locale**

1. Update app configurations
    - Languages Selector
    - Insert into the website header
    - Select Theme: Minimal
    - Shop default language
    - Save
2. To start the test, run:
    
    ```html
    pytest -vs testcases/test_locale_selector_shop_base.py
    ```
    
3. Update app configurations
    - **Float and always seen**
    - To start the test, run the same command

## Selected Locale

1. Update app configurations
    - Languages Selector
    - **Insert into the website header**
    - Select Theme: Minimal
    - Default Display Language: **Deutsch** (based on my `config.json`)
2. To start the test, run:
    
    ```html
    pytest -vs testcases/test_locale_selector_selected.py
    ```
    
3. Update app configurations
    - **Float and always seen**
    - To start the test, run the same command

## **Browser Language**

1. Update app configurations
    - Languages Selector
    - **Insert into the website header**
    - Select Theme: Minimal
    - **Customer's browser language**
    - Recommendation Popup: ****OFF
    - Save
2. To start the test, run:
    
    ```html
    pytest -vs testcases/test_locale_selector_browser_base.py
    ```
    
3. Update app configurations
    - **Float and always seen**
    - To start the test, run the same command

## Recommendation Popup

1. Update app configurations
    - Languages Selector
    - **Insert into the website header**
    - Select Theme: Minimal
    - Customer's browser language
    - **Recommendation Popup: ON**
    - Save
2. To start the test, run:
    
    ```html
    pytest -vs testcases/test_locale_selector_recommendation.py
    ```
    
3. Update app configurations
    - **Float and always seen**
    - To start the test, run the same command

# Region Selector

## Base Region

1. Update app configurations
    - Regions/Currencies Selector
    - **Insert into the website header**
    - Select Theme: Minimal
    - **Default Region/Currency: Canada (the shop base region)**
    - Save
2. To start the test, run:
    
    ```html
    pytest -vs testcases/test_region_selector_shop_base.py
    ```
    
3. Update app configurations
    - **Float and always seen**
    - To start the test, run the same command

## Selected Region

1. Update app configurations
    - Regions/Currencies Selector
    - **Insert into the website header**
    - Select Theme: Minimal
    - **Default Region/Currency: United States** (based on `config.json`)
2. To start the test, run:
    
    ```html
    pytest -vs testcases/test_region_selector_selected.py
    ```
    
3. Update app configurations
    - **Float and always seen**
    - To start the test, run the same command

## Geo-Based Region

1. Update app configurations
    - Regions/Currencies Selector
    - **Insert into the website header**
    - Select Theme: Minimal
    - **Default Region/Currency: Customer's geo location based**
    - Recommendation Popup: OFF
2. Turn on VPN with US IP address (based on `config.json`)
3. To start the test, run:
    
    ```html
    pytest -vs testcases/test_region_selector_geo_based.py
    ```
    
4. Update app configurations
    - **Float and always seen**
    - To start the test, run the same command

## Recommendation Popup

1. Update app configurations
    - Regions/Currencies Selector
    - **Insert into the website header**
    - Select Theme: Minimal
    - Default Region/Currency: Customer's geo location based
    - **Recommendation Popup: ON**
2. Turn on VPN with US IP address (based on `config.json`)
3. To start the test, run:
    
    ```html
    pytest -vs testcases/test_region_selector_recommendation.py
    ```
    
4. Update app configurations
    - **Float and always seen**
    - To start the test, run the same command

# Locale and Region Selector

## Store Locale + Store Region

1. Update app configurations
    - Languages and Regions/Currencies Selector
    - **Dropdown**
    - Insert into the website header
    - Select Theme: Minimal
    - **Default Region/Currency: Canada** (store base region)
    - **Default Display Language: Shop default language**
    - Save
2. To start the test, run:
    
    ```html
    pytest -vs testcases/test_locale_and_region_selector_base.py
    ```
    
3. Update app configurations
    - **Float and always seen**
    - To start the test, run the same command

## Selected Locale + Selected Region

1. Update app configurations
    - Languages and Regions/Currencies Selector
    - Dropdown
    - Insert into the website header
    - Select Theme: Minimal
    - **Default Region/Currency: United States** (based on `config.json`)
    - **Default Display Language: Deutsch** (based on `config.json`)
    - Save
2. To start the test, run:
    
    ```html
    pytest -vs testcases/test_locale_and_region_selector_selected.py
    ```
    
3. Update app configurations
    - **Float and always seen**
    - To start the test, run the same command

## Browser Locale + Geo Region

1. Update app configurations
    - Languages and Regions/Currencies Selector
    - Dropdown
    - Insert into the website header
    - Select Theme: Minimal
    - **Default Region/Currency: Customer's geo location based**
    - **Default Display Language**: **Customer's browser language**
    - **Recommendation Popup: OFF**
    - Save
2. To start the test, run:
    
    ```html
    pytest -vs testcases/test_locale_and_region_selector_browser_geo.py
    ```
    
3. Update app configurations
    - **Float and always seen**
    - To start the test, run the same command

## Recommendation Popup

1. Update app configurations
    - Languages and Regions/Currencies Selector
    - Dropdown
    - Insert into the website header
    - Select Theme: Minimal
    - Default Region/Currency: Customer's geo location based
    - Default Display Language: Customer's browser language
    - **Recommendation Popup: ON**
    - Save
2. To start the test, run:
    
    ```html
    pytest -vs testcases/test_locale_and_region_selector_recommendation.py
    ```
    
3. Update app configurations
    - **Float and always seen**
    - To start the test, run the same command

# Geolocation App by Shopify

1. Update app configurations
    - Languages and Regions/Currencies Selector
    - **Default Region/Currency: Canada** (store base region)
    - **Default Display Language: Shop default language**
    - Save
2. Turn on the Geolocation app
    - Go to the Geolocation app
    - Show recommendations
3. To start the test, run:
    
    ```html
    pytest -vs testcases/test_geolocation_app_recommend.py
    ```
    
4. Update app configurations
    - **Languages Selector**
    - Save
    - Run the same tests
5. Update app configurations
    - **Regions/Currencies Selector**
    - Run the same tests
6. Turn off the Geolocation app

# Notes
Update config.json based on your test store.
- `locales` and `regions` data needs to be in its original format. For example, the `zh-TW` should not be `zh-tw`.
- `locales` data requires at least 2 items.
- `locales` should not include `manual_selected_locale`.
- `regions` should not include `manual_selected_region`.